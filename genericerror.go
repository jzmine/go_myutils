package go_myutils

type GenericError struct {
	Err error
}

func (e *GenericError) Error() string {
	return "Generic error raised!"
}
