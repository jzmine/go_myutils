package go_myutils

import (
	"database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/mikespook/gorbac"
)

var (
	AppRbac *gorbac.RBAC
)

func SetupRbac(dBOrm *gorm.DB) (err error) {
	AppRbac = gorbac.New()

	//get all roles
	roles, err := dBOrm.Raw(`select rbac_roles_id, role_name from rbac_roles where deleted_at is null
    and parent_id is null `).Rows()
	defer roles.Close()

	for roles.Next() {
		var roleid int64
		var rolename string
		roles.Scan(&roleid, &rolename)
		//get all perms for this role
		var perms *sql.Rows
		perms, err = dBOrm.Raw(`select url, trim(both from action) from vw_roles_permissions
      where rbac_roles_id = ? `, roleid).Rows()
		if err == nil {
			defer perms.Close()
			rrole := gorbac.NewStdRole(rolename) //role only added if it has permissions
			for perms.Next() {
				var sperm, saction string
				perms.Scan(&sperm, &saction)
				//admin := NewLayerPermission("admin")
				//rperm := gorbac.NewStdPermission(sperm)
				// //admindashboard := NewLayerPermission("admin:dashboard")
				if saction == "" {
					rperm := gorbac.NewLayerPermission(sperm + ":r") //add the root permission as a read action
					rrole.Assign(rperm)                              //add d permission to the role
				} else {
					rpermact := gorbac.NewLayerPermission(fmt.Sprintf("%s:%s", sperm, saction))
					rrole.Assign(rpermact) //add d permission to the role
				}
			}
			AppRbac.Add(rrole) //ok store it
		}
	}

	return
}
