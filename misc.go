package go_myutils

import (
	"crypto/md5"
	"crypto/sha512"
	"encoding/hex"
	"github.com/matoous/go-nanoid"
	"github.com/satori/go.uuid"
)

func GetUUIDMD5Hash() string {
	u1 := uuid.NewV4()
	hasher := md5.New()
	hasher.Write([]byte(u1.String()))
	return hex.EncodeToString(hasher.Sum(nil))
}

func StringMD5(str string) string {
	hasher := md5.New()
	hasher.Write([]byte(str))
	return hex.EncodeToString(hasher.Sum(nil))
}

func RandomUUIDMD5Hash() string {
	return StringMD5(GetUUIDMD5Hash())
}

func GetUUIDSHA512Hash() string {
	u1 := uuid.NewV4()
	hasher := sha512.New()
	hasher.Write([]byte(u1.String()))
	return hex.EncodeToString(hasher.Sum(nil))
}

func StringSHA512Hash(str string) string {
	hasher := sha512.New()
	hasher.Write([]byte(str))
	return hex.EncodeToString(hasher.Sum(nil))
}

func GetNanoId() string {
	id, _ := gonanoid.Generate("123456789ABCDEFGHJKMNPQRSTUVWXYZ0LI", 15)
	return id
}

func GetNanoIDSession() string {
	id, _ := gonanoid.Generate("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", 25)
	return id
}
