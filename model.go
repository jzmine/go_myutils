package go_myutils

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/cache"
	"github.com/jinzhu/gorm"
	"strings"
	"time"
	//"github.com/garyburd/redigo/redis"
	"errors"
	"github.com/gomodule/redigo/redis"
)

/*
type DBUtils interface {
  GetPkValue(model interface{}, uid string) (pkId int64, err error)
  GetUidValue(model interface{}, pkvalue int64) (uid string, err error)
  GetPkValueByField(model interface{}, whereFieldName string, whereFieldValue interface{}) (pkId int64, err error)
}
*/

var (
	ErrPGNoRows = errors.New("sql: no rows in result set")
)

type Model struct {
	Uid string `gorm:"default:null" json:"id" form:"id"`
	//Active bool
	CreatedAt time.Time   `gorm:"type:timestamp" json:"-"`
	UpdatedAt time.Time   `gorm:"type:timestamp" json:"-"`
	DeletedAt driver.Null `gorm:"type:timestamp" json:"-"`
	/* driver.Null// time.Time `json:"-"` */
}

type GormModel struct {
	Uid string `gorm:"default:null" json:"id" form:"id"`
	//Active bool
	CreatedAt time.Time   `gorm:"type:timestamp" json:"-"`
	UpdatedAt time.Time   `gorm:"type:timestamp" json:"-"`
	DeletedAt time.Time `gorm:"type:timestamp;index" json:"-"`
}


type FBActive struct {
	IsActive bool   `json:"-" form:"-"`
	Active   string `gorm:"-" json:"Active" form:"Active" `
}

func (fba *FBActive) UpdateIsActive() {
	if fba.Active == "No" {
		fba.IsActive = false
	} else if fba.Active == "Yes" {
		fba.IsActive = true
	}
}

func (fba *FBActive) UpdateActive() {
	if fba.IsActive {
		fba.Active = "Yes"
	} else {
		fba.Active = "No"
	}
}

/*func (fba FBActive) GetIsActive() bool {
  return fba.IsActive
}*/

func (fba *FBActive) SetIsActive(active *bool) {
	if active == nil || *active == false {
		fba.Active = "No"
		fba.IsActive = false
	} else {
		fba.Active = "Yes"
		fba.IsActive = true
	}
}

/*
func (fba FBActive) GetActive() string {
  return fba.Active
} */

func (fba *FBActive) SetActive(isactive *string) {
	if isactive == nil || *isactive == "No" {
		fba.Active = "No"
		fba.IsActive = false
	} else {
		fba.Active = "Yes"
		fba.IsActive = true
	}
}

type FBTime []byte

func (t FBTime) Time() (time.Time, error) {
	return time.Parse("15:04:05", string(t))
}

//Mon Jan 2 15:04:05 MST 2006 or 01/02 03:04:05PM ‘06 -0700
type FBDate []byte

func (t FBDate) Date() (time.Time, error) {
	return time.Parse("02-Jan-2006", string(t))
}


//https://blog.charmes.net/post/json-dates-go/
//https://github.com/mattn/go-sqlite3/issues/190#issuecomment-343341834
type CustomTime struct {
	time.Time
}

type CustomDate struct {
	time.Time
}

//region customTime
const CustomTimeLayout = "3:4 PM"

func (ct *CustomTime) UnmarshalJSON(b []byte) (err error) {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}
	ct.Time, err = time.Parse(CustomDateLayout, string(b))
	return
}

func (ct *CustomTime) MarshalJSON() ([]byte, error) {
	return []byte(ct.Time.Format(CustomDateLayout)), nil
}

func (j CustomTime) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *CustomTime) Scan(value interface{}) error {
	tm := value.(time.Time)
	byt := []byte(tm.Format(CustomTimeLayout))
	if err := json.Unmarshal(byt, &j); err != nil {
		return err
	}
	return nil
}

//endregion

//region customtimepg
type CustomTimePG struct {
	time.Time
}

const CustomTimeLayoutPG = "15:04:05"

func (ct *CustomTimePG) UnmarshalJSON(b []byte) (err error) {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}
	ct.Time, err = time.Parse(CustomTimeLayoutPG, string(b))
	return
}

func (ct *CustomTimePG) MarshalJSON() ([]byte, error) {
	//println(string([]byte(ct.Time.Format(CustomTimeLayoutPG))))
	return []byte(`"` + ct.Time.Format(CustomTimeLayoutPG) + `"`), nil
}

func (j CustomTimePG) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *CustomTimePG) Scan(value interface{}) error {
	tm := value.(time.Time)
	//byt := []byte(tm.Format(CustomTimeLayoutPG))
	//println(string(byt))
	j.Time = tm
	return nil

	/*tm := value.(time.Time)
	byt := []byte(tm.Format(CustomTimeLayoutPG))
	println(string(byt))
	if err := json.Unmarshal(byt, &j); err != nil {
		return err
	}
	return nil*/
}

//endregion customtimepg

//region customdatetimestamppg
type CustomDateTimePG struct {
	time.Time
}

//12/4/2019 12:41:23 PM
//Mon Jan 2 15:04:05 MST 2006 or 01/02 03:04:05PM ‘06 -0700
const CustomDateTimeLayoutPG = "01/02/2006 15:04:05 PM"

func (ct *CustomDateTimePG) UnmarshalJSON(b []byte) (err error) {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}
	ct.Time, err = time.Parse(CustomDateTimeLayoutPG, string(b))
	return
}

func (ct *CustomDateTimePG) MarshalJSON() ([]byte, error) {
	//println(string([]byte(ct.Time.Format(CustomTimeLayoutPG))))
	if ct.IsZero() {
		return []byte("null"), nil
	}
	return []byte(`"` + ct.Time.Format(CustomDateTimeLayoutPG) + `"`), nil
}

func (j CustomDateTimePG) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *CustomDateTimePG) Scan(value interface{}) error {
	if value == nil {
		return nil
	}
	tm := value.(time.Time)
	//byt := []byte(tm.Format(CustomTimeLayoutPG))
	//println(string(byt))
	j.Time = tm
	return nil

	/*tm := value.(time.Time)
	byt := []byte(tm.Format(CustomTimeLayoutPG))
	println(string(byt))
	if err := json.Unmarshal(byt, &j); err != nil {
		return err
	}
	return nil*/
}

//endregion customdatetimestamppg

//region customtime

const CustomDateLayout = "2006-01-02" //"02/Jan/2006"
func (j CustomDate) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *CustomDate) Scan(value interface{}) error {
	tm := value.(time.Time)
	j.Time = tm
	/*byt := []byte(tm.Format(CustomDateLayout))
	if err := json.Unmarshal(byt, &j); err != nil {
	  return err
	}*/
	return nil

	//tm := value.(time.Time)
	//j.Time = tm
	/*byt := []byte(tm.Format(CustomDateLayout))
	fmt.Printf("%s", byt)
	if err := json.Unmarshal(byt, &j); err != nil {
	  return err
	}*/
	return nil
}

func (ct *CustomDate) UnmarshalJSON(b []byte) (err error) {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}
	ct.Time, err = time.Parse(CustomDateLayout, string(b))
	return
}

func (ct *CustomDate) MarshalJSON() ([]byte, error) {
	return json.Marshal(ct.Time.Format(CustomDateLayout))
	return []byte(ct.Time.Format(CustomDateLayout)), nil
}

//endregion

//region custom_datetime_flutter
type CustomDateTimeFlutter struct {
	time.Time
}

const CustomDateTimeFlutterLayout = "2006-01-02T15:04:05" //"02/Jan/2006"
func (j CustomDateTimeFlutter) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *CustomDateTimeFlutter) Scan(value interface{}) error {
	tm := value.(time.Time)
	j.Time = tm
	/*byt := []byte(tm.Format(CustomDateLayout))
	if err := json.Unmarshal(byt, &j); err != nil {
	  return err
	}*/
	return nil

	//tm := value.(time.Time)
	//j.Time = tm
	/*byt := []byte(tm.Format(CustomDateLayout))
	fmt.Printf("%s", byt)
	if err := json.Unmarshal(byt, &j); err != nil {
	  return err
	}*/
	return nil
}

func (ct *CustomDateTimeFlutter) UnmarshalJSON(b []byte) (err error) {
	if b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}
	ct.Time, err = time.Parse(CustomDateTimeFlutterLayout, string(b))
	return
}

func (ct *CustomDateTimeFlutter) MarshalJSON() ([]byte, error) {
	return json.Marshal(ct.Time.Format(CustomDateTimeFlutterLayout))
	return []byte(ct.Time.Format(CustomDateTimeFlutterLayout)), nil
}

func DateTimeToFlutter(dt time.Time) string {
	return dt.Format(CustomDateTimeFlutterLayout)
}

func FlutterToDateTime(str string) (CustomDateTimeFlutter, error) {
	var t CustomDateTimeFlutter
	var err error
	t.Time, err = time.Parse(CustomDateTimeFlutterLayout, str)
	return t, err
}

//custom_datetime_flutter

func (model *Model) BeforeCreate(scope *gorm.Scope) {
	if model.Uid == "" {
		scope.SetColumn("Uid", GetUUIDMD5Hash())
	}
}

func GetPkValue(db *gorm.DB, model interface{}, uid string) (pkId int64, err error) {
	scope := db.NewScope(model)
	row := db.Table(scope.TableName()).Select(scope.PrimaryKey()).Where("uid = ?", uid).Row()
	err = row.Scan(&pkId)
	return
}

func GetSequenceValue(db *gorm.DB, sequencename string) (sequenceid int64, err error) {
	row := db.Raw("select next value for " + sequencename + " from rdb$database").Row()
	err = row.Scan(&sequenceid)
	return
}

func GetUidValue(db *gorm.DB, model interface{}, pkvalue int64) (uid string, err error) {
	scope := db.NewScope(model)
	row := db.Table(scope.TableName()).Select("uid").Where(fmt.Sprintf("%s = ?", scope.PrimaryKey()), pkvalue).Row()
	err = row.Scan(&uid)
	return
}

func GetPkValueByField(db *gorm.DB, model interface{}, whereFieldName string, whereFieldValue interface{}) (pkId int64, err error) {
	scope := db.NewScope(model)
	row := db.Table(scope.TableName()).Select(scope.PrimaryKey()).Where(fmt.Sprintf("%s = ?", whereFieldName), whereFieldValue).Row()
	err = row.Scan(&pkId)
	return
}

func GetFieldValue(db *gorm.DB, model interface{}, whereFieldName string, whereFieldValue interface{},
	resultField string) (resultValue interface{}, err error) {
	scope := db.NewScope(model)
	row := db.Table(scope.TableName()).Select(resultField).Where(fmt.Sprintf("%s = ?", whereFieldName), whereFieldValue).Row()
	err = row.Scan(&resultValue)
	return
}

func GetSingleSQLValue(db *gorm.DB, sql string, value ...interface{}) (resultValue interface{}, err error) {
	row := db.Raw(sql, value).Row()
	err = row.Scan(&resultValue)
	return
}

func GetSingleSQLInt64Value(db *gorm.DB, sql string, value ...interface{}) (resultValue int64, err error) {
	row := db.Raw(sql, value).Row()
	err = row.Scan(&resultValue)
	return
}

func GetLookupStringId(db *gorm.DB, tablename, resultfieldname, lookupfieldname string,
	lookupfieldvalue interface{}) (idValue int64, err error) {
	if s, ok := lookupfieldvalue.(string); ok && s == "" {
		return -1, errors.New("Invalid lookup value")
	}
	row := db.Raw(fmt.Sprintf(`select %s from %s where %s = ?`, resultfieldname, tablename, lookupfieldname),
		lookupfieldvalue).Row()
	err = row.Scan(&idValue)
	return
}

func JsonArrayGetData(db *gorm.DB, model interface{}, dbFieldName, arrayFieldName string) (
	ja []map[string]interface{}, err error) {
	scope := db.NewScope(model)
	rows, err := db.Table(scope.TableName()).Select(dbFieldName).Rows()
	defer rows.Close()
	var val interface{}
	for rows.Next() {
		rows.Scan(&val)
		ja = append(ja, map[string]interface{}{arrayFieldName: val})
	}
	return
}

func SQLToJSONMap(rows *sql.Rows) ([]map[string]interface{}, error) {
	//map[int]map[string]interface{}
	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}
	return tableData, nil
	//return json.Marshal(tableData)
}

func ArrayGetData(db *gorm.DB, model interface{}, dbFieldName, orderFields string) (
	ja []string, err error) {
	scope := db.NewScope(model)
	rows, err := db.Table(scope.TableName()).Select(dbFieldName).Order(orderFields).Rows()
	defer rows.Close()
	var val string
	for rows.Next() {
		rows.Scan(&val)
		ja = append(ja, val)
	}
	return
}

func ArrayGetDataRows(db *gorm.DB, sqlstr string, values interface{}) (ja []string, err error) {
	var rows *sql.Rows
	if values != nil {
		rows, err = db.Raw(sqlstr, values).Rows()
	} else {
		rows, err = db.Raw(sqlstr).Rows()
	}
	defer rows.Close()
	var val string
	//ja = make([]string, 0)
	for rows.Next() {
		rows.Scan(&val)
		ja = append(ja, val)
	}
	return
}

/*
func SQLToJSArray(rows *sql.Rows) ([]byte, error) {
  columns, err := rows.Columns()
  if err != nil {
    return nil, err
  }
  if columns > 1 {
    return nil, errors.New("Too many fields!")
  }
  count := 1
  tableData := make([]map[string]interface{}, 0)
  values := make([]interface{}, count)
  valuePtrs := make([]interface{}, count)
  for rows.Next() {
     valuePtrs[0] = &values[i]
    }
    rows.Scan(valuePtrs...)
    entry := make(map[string]interface{})
    for i, col := range columns {
      var v interface{}
      val := values[i]
      b, ok := val.([]byte)
      if ok {
        v = string(b)
      } else {
        v = val
      }
      entry[col] = v
    }
    tableData = append(tableData, entry)
  }
  return json.Marshal(tableData)
}*/

func CacheQueryData(db *gorm.DB, cacher interface{}, sqlstr, cacheKey string, duration time.Duration) (cacheddata []byte, err error) {
	var rows *sql.Rows
	if rows, err = db.Raw(sqlstr).Rows(); err == nil {
		if data, err := SQLToJSONMap(rows); err == nil {
			//var js string
			if cacheddata, err = json.Marshal(data); err == nil {
				//handle beego's cache
				_, ok := cacher.(cache.Cache)
				if ok {
					cacher.(cache.Cache).Put(cacheKey, cacheddata, duration)
				}
			}
		}
	}

	return cacheddata, err
}

func CacheQueryData_V2(db *gorm.DB, cacher interface{}, sqlstmt string, duration time.Duration,
	values ...interface{}) (cacheddata []byte, err error) {
	var cacheKey string
	cacheKey = StringMD5(sqlstmt + fmt.Sprintf("%v", values))
	//check if already cached
	intf := cacher.(cache.Cache).Get(cacheKey)
	if intf != nil {
		cachestr, err := redis.String(intf, nil)
		return []byte(cachestr), err
	}
	var rows *sql.Rows
	if len(values) <= 0 {
		rows, err = db.Raw(sqlstmt, values).Rows()
	} else {
		rows, err = db.Raw(sqlstmt).Rows()
	}
	if err == nil {
		if data, err := SQLToJSONMap(rows); err == nil {
			//var js string
			if cacheddata, err = json.Marshal(data); err == nil {
				//handle beego's cache
				_, ok := cacher.(cache.Cache)
				if ok {
					cacher.(cache.Cache).Put(cacheKey, cacheddata, duration)
				}
			}
		}
	}

	return cacheddata, err
}

func GetPGJSONData(db *gorm.DB, sql string, values ...interface{}) (data []map[string]interface{}, err error) {
	row := db.Raw(sql, values).Row()
	var jsd []byte
	err = row.Scan(&jsd)
	if err = json.Unmarshal(jsd, &data); err != nil {
		return []map[string]interface{}{}, err
	}
	return data, err
}

func GetPGJSONDataWithValues(db *gorm.DB, sql string, values ...interface{}) (data []map[string]interface{}, err error) {
	row := db.Raw(sql, values).Row()
	var jsd []byte
	err = row.Scan(&jsd)
	if err = json.Unmarshal(jsd, &data); err != nil {
		return []map[string]interface{}{}, err
	}
	return data, err
}

func GetSqlAsPGJSONArrayData(db *gorm.DB, sqlstmt string, values ...interface{}) (data []map[string]interface{}, err error) {
	var row *sql.Row
	if len(values) <= 0 {
		row = db.Raw(sqlstmt).Row()
	} else {
		row = db.Raw(sqlstmt, values).Row()
	}
	var jsd []byte
	if err = row.Scan(&jsd); err != nil {
		return data, err
	}
	err = json.Unmarshal(jsd, &data)
	if err != nil && strings.ToLower(err.Error()) == "unexpected end of json input" {
		return []map[string]interface{}{}, nil
	}
	return data, err
}

func GetSqlAsPGJSONArrayData_v2(db *gorm.DB, sqlstmt string, values []interface{}) (data []map[string]interface{}, err error) {
	var row *sql.Row
	if len(values) <= 0 {
		row = db.Raw(sqlstmt).Row()
	} else {
		row = db.Raw(sqlstmt, values).Row()
	}
	var jsd []byte
	if err = row.Scan(&jsd); err != nil {
		return data, err
	}
	err = json.Unmarshal(jsd, &data)
	if err != nil && strings.ToLower(err.Error()) == "unexpected end of json input" {
		return []map[string]interface{}{}, nil
	}
	return data, err
}

func GetRowAsPGJSONArrayData(row *sql.Row) (data []map[string]interface{}, err error) {
	var jsd []byte
	err = row.Scan(&jsd)
	err = json.Unmarshal(jsd, &data)
	if err != nil && strings.ToLower(err.Error()) == "unexpected end of json input" {
		return []map[string]interface{}{}, nil
	}
	return data, err
}

func GetRowAsPGJSONData2(row *sql.Row) (data map[string]interface{}, err error) {
	var jsd []byte
	err = row.Scan(&jsd)
	if err = json.Unmarshal(jsd, &data); err != nil {
		return map[string]interface{}{}, err
	}
	return data, err
}

func GetRowAsPGJSONData(db *gorm.DB, sql string, values ...interface{}) (data map[string]interface{}, err error) {
	row := db.Raw(sql, values).Row()
	var jsd []byte
	err = row.Scan(&jsd)
	if err = json.Unmarshal(jsd, &data); err != nil {
		return map[string]interface{}{}, err
	}
	return data, err
}

func DeleteRecord(db *gorm.DB, table string, idfield string, idvalue interface{}) (err error) {
	//check if record exists
	row := db.Raw(fmt.Sprintf(`select %s from %s where %s = ?`, idfield, table, idfield), idvalue).Row()
	var val interface{}
	if err = row.Scan(&val); err != nil {
		return err
	}
	//now delete
	err = db.Exec(fmt.Sprintf(`delete from %s where %s = ?`, table, idfield), val).Error
	return
}
