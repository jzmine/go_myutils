package jobber

import (
	"github.com/albrow/jobs"
	"github.com/caarlos0/env"
	"gopkg.in/gomail.v2"
)

type JobsMailerData struct {
	FromAddress, FromName        string
	ToAddresses                  []string
	CCAddresses                  []string
	Subject                      string
	BodyContentType, BodyMessage string
}

type mailConfig struct {
	Username string `env:"SEND_MAIL_USERNAME,required"`
	Password string `env:"SEND_MAIL_PASSWORD,required"`
	Host     string `env:"SEND_MAIL_HOST,required"`
	Port     int    `env:"SEND_MAIL_PORT,required"`
}

var (
	JobsMailer *jobs.Type
	mc         mailConfig
)

func init() {
	//jobs.Config.Db.Database = 10

	var err error

	err = env.Parse(&mc)
	if err != nil {
		panic(err)
	}

	JobsMailer, err = jobs.RegisterType("jobsmailer", 3, handleJobsMailer)

	if err != nil {
		panic(err)
	}

}

func handleJobsMailer(jmd JobsMailerData) error {
	//get params from env
	d := gomail.NewDialer(mc.Host, mc.Port, mc.Username, mc.Password)
	m := gomail.NewMessage()
	m.SetAddressHeader("From", jmd.FromAddress, jmd.FromName)
	m.SetHeader("Cc", jmd.CCAddresses...)
	m.SetHeader("To", jmd.ToAddresses...)
	m.SetHeader("Subject", jmd.Subject)
	m.SetBody(jmd.BodyContentType, jmd.BodyMessage)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	println("mail sent to: ", jmd.ToAddresses)
	return nil
}
