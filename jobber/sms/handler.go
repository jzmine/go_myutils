package sms

import (
	"bitbucket.org/jzmine/go_myutils"
	"fmt"
	"github.com/albrow/jobs"
	"github.com/astaxie/beego/httplib"
	"github.com/go-ini/ini"
	"github.com/jinzhu/gorm"
	"net/url"
	"strings"
)

type SMSerData struct {
	GSMNos    []string
	Msg       string
	UsersId   int64
	ExtraData string
}

type smser struct {
	Url              string
	Username         string
	Password         string
	Sender           string
	API              string
	DbName, DbDriver string
}

var (
	JobsSMSer *jobs.Type
	_smser    *smser
	_db       *gorm.DB = nil
)

func init() {
	var err error
	var cfg *ini.File

	JobsSMSer, err = jobs.RegisterType("jobsmser", 3, handleSMSer)

	cfg, err = ini.InsensitiveLoad("conf/smser.ini")
	if err != nil {
		panic(err)
	}

	_smser = new(smser)
	if err = cfg.Section("sms").MapTo(_smser); err != nil {
		panic(err)
	}
	//now create db if we have a driver value and name
	if _smser.DbDriver != "" && _smser.DbName != "" {
		if _db, err = gorm.Open(_smser.DbDriver, _smser.DbName); err != nil {
			panic(err)
		}
		_db.LogMode(true)
		_db.DB().SetMaxIdleConns(10)
		_db.DB().SetMaxOpenConns(5)
		_db.SingularTable(true)
		if err = _db.DB().Ping(); err != nil {
			print(err)
		}
	}
	println("jobsSMSer initted!")
}

func handleSMSer(smsdata []SMSerData) error {
	for _, smsd := range smsdata {
		for ndx, val := range smsd.GSMNos {
			if len(val) > 10 {
				smsd.GSMNos[ndx] = "234" + val[1:]
			}
		}

		//"http://98.102.204.231/smsapi/Send.aspx?UN=jasmine&P=7tGF676E&SA=iDonTell"
		var url string
		if _smser.API == "utiware" {
			url = getUtilWareSMS(smsd)
		} else if _smser.API == "cheapglobal" {
			url = getCheapGlobalSMS(smsd)
		}
		//save into db
		var uid string
		if _db != nil {
			uid = go_myutils.GetUUIDMD5Hash()
			err := _db.Exec(`insert into send_sms(message,recipient_user_id,url_data,uid)values(?,?,?,?)`,
				smsd.Msg, smsd.UsersId, url, uid).Error
			//continue even if error saving to db
			if err != nil {
				print(err)
			}
		}

		println("sending to: ", url)
		req := httplib.Get(url)
		str, err := req.String()
		if uid != "" && _db != nil {
			//save response
			_db.Exec(`update send_sms set callback_resp = ? where uid = ?`, str, uid)
		}
		fmt.Printf("send response: %v\n%v", str, err)
	}

	return nil
}

func getUtilWareSMS(smsdata SMSerData) string {
	destno := strings.Join(smsdata.GSMNos, ",")
	params := url.Values{}
	params.Add("M", smsdata.Msg)
	url := fmt.Sprintf("%s?UN=%s&P=%s&SA=%s", _smser.Url, _smser.Username, _smser.Password, _smser.Sender)
	if len(smsdata.Msg) > 160 {
		url += "&L=1"
	}
	url = fmt.Sprintf("%s&DA=%s&%s", url, destno, params.Encode())
	return url
}

func getCheapGlobalSMS(smsdata SMSerData) string {
	destno := strings.Join(smsdata.GSMNos, ",")
	params := url.Values{}
	params.Add("message", smsdata.Msg)
	url := fmt.Sprintf("%s?sub_account=%s&sub_account_pass=%s&action=send_sms&sender_id=%s&recipients=%s&%s",
		_smser.Url, _smser.Username, _smser.Password, _smser.Sender, destno, params.Encode())
	if smsdata.ExtraData != "" {
		url = url + smsdata.ExtraData
	}
	return url
}
