package defines

import "errors"

var (
	ErrBlank = errors.New("")
)
