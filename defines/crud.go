package defines

import "errors"

const (
	CRUD_CREATE = "create"
	CRUD_READ   = "read"
	CRUD_UPDATE = "update"
	CRUD_DELETE = "delete"
)

var (
	ErrInvalidRecordId    = errors.New("")
	ErrInvalidRequestData = errors.New("Invalid Request Data!")
	ErrIssueWithRequest   = errors.New("There was an issue completing your request. Try again later")
	ErrLogin              = errors.New("There was an error logging you in. Please try again later!")
)
