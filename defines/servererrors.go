package defines

import "github.com/pkg/errors"

var (
	LoginError = errors.New("Error during login. Please try again later.")
)
