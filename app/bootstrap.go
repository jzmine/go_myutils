package app

import (
	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
)

func SetupDB(drivername, dbparams string) (dbh *gorm.DB, err error) {
	if dbh, err = gorm.Open("postgres", dbparams); err != nil {
		panic(err)

	}
	if err = dbh.DB().Ping(); err != nil {
		panic(err)
	}
	if err == nil {
		dbh.LogMode(true)
		dbh.DB().SetMaxIdleConns(10)
		dbh.DB().SetMaxOpenConns(100)
		dbh.SingularTable(true)
	}

	println(drivername + " DB Setup")

	return dbh, nil
}

func SetupCache(drivername, rediscon string) (thecache cache.Cache) {
	var err error
	thecache, err = cache.NewCache(drivername, rediscon)
	if err != nil {
		panic(err)
	}
	println(drivername + " Cache ready!")
	return thecache
}
