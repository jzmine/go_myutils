package go_myutils

const (
	CRUD_CREATE = "new"
	CRUD_READ   = "get"
	CRUD_UPDATE = "upd"
	CRUD_DELETE = "del"
	SHOW_ALERT  = "SHOW_ALERT"
)

type ShowAlert struct {
	Title, Type, Message string
}
