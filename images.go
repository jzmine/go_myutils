package go_myutils

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"github.com/nfnt/resize"
	"os"
	"path/filepath"
	//"image/png"
	"image"
	"image/jpeg"
)

func ImageTo64(filename string) (image64 string, err error) {
	var imgFile *os.File
	imgFile, err = os.Open(filename) // a QR code image
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	defer imgFile.Close()

	// create a new buffer base on file size
	fInfo, _ := imgFile.Stat()
	var size int64 = fInfo.Size()
	buf := make([]byte, size)

	// read file content into buffer
	fReader := bufio.NewReader(imgFile)
	fReader.Read(buf)
	// convert the buffer bytes to base64 string - use buf.Bytes() for new image
	image64 = base64.StdEncoding.EncodeToString(buf)
	return image64, nil
}

func CreateImageThumbnail(sourcefile, destinationfile string) (err error) {
	/*err := func() error {
	    var err error
	    //create thumbnail folder if not existing
	    destpath := filepath.Dir(destinationfile)
	    if _, err := os.Stat(destpath);os.IsNotExist(err) {
	      os.Mkdir(destpath, os.ModePerm)
	    }
	    //load and decode sourcefile
	    var srcFile *os.File
	    if srcFile, err = os.Open(sourcefile); err != nil { return err }
	    var img image.Image
	    if img, err = png.Decode(srcFile); err != nil { return err }
	    srcFile.Close()

	    img = resize.Thumbnail(128,128,img, resize.Lanczos3)
	    var out *os.File
	    if out, err = os.Create(destinationfile); err != nil { return err }
	    defer out.Close()

	    if err = png.Encode(out, img); err != nil { return err }

	    return nil
	  }()
	  return err*/
	//var err error
	//create thumbnail folder if not existing
	destpath := filepath.Dir(destinationfile)
	if _, err := os.Stat(destpath); os.IsNotExist(err) {
		os.Mkdir(destpath, os.ModePerm)
	}
	//load and decode sourcefile
	var srcFile *os.File
	if srcFile, err = os.Open(sourcefile); err != nil {
		return err
	}
	var img image.Image
	if img, err = jpeg.Decode(srcFile); err != nil {
		return err
	}
	srcFile.Close()

	img = resize.Thumbnail(128, 128, img, resize.Lanczos3)
	var out *os.File
	if out, err = os.Create(destinationfile); err != nil {
		return err
	}
	defer out.Close()

	if err = jpeg.Encode(out, img, &jpeg.Options{Quality: 100}); err != nil {
		return err
	}

	return nil
}
