package go_myutils

import (
	"errors"
	"net"
	"net/url"
	"syscall"
)

func GetNetworkErrorString(err error) string  {
	if err == nil {
		return ""
	} else if netError, ok := err.(net.Error); ok && netError.Timeout() {
		return "connection timeout"
	}

	erp, ok := err.(*url.Error)
	if ok {
		//var ope net.OpError
		ope := erp.Err
		switch t := ope.(type) {
		case *net.OpError:
			if t.Op == "dial" {
				return "Unknown host"
			} else if t.Op == "read" {
				return "Connection refused"
			}
		case syscall.Errno:
			if t == syscall.ECONNREFUSED {
				return "Connection refused"
			}
			if t == syscall.ECONNABORTED {
				return "Connection aborted"
			}
			if t == syscall.ECONNRESET {
				return "Connection reset"
			}
		}
	}

		switch t := err.(type) {
		case *net.OpError:
			if t.Op == "dial" {
				return "Unknown host or connection error"
			} else if t.Op == "read" {
				return "Connection refused"
			}
		case syscall.Errno:
			if t == syscall.ECONNREFUSED {
				return "Connection refused"
			}
			if t == syscall.ECONNABORTED {
				return "Connection aborted"
			}
			if t == syscall.ECONNRESET {
				return "Connection reset"
			}

	}

	return "unknown network error"
}

func GetNetworkErr(err error) error  {
	error := GetNetworkErrorString(err)
	if error == "" {
		return nil
	}
	return errors.New(error)
}