package conf

import (
	"github.com/go-ini/ini"
)

type appconf struct {
	AppUrl string
}

var (
	AppConf *appconf
)

func init() {
	cfg, err := ini.InsensitiveLoad("conf/app.conf")
	if err != nil {
		panic(err)
	}

	AppConf = new(appconf)
	if err = cfg.MapTo(AppConf); err != nil {
		panic(err)
	}
}
