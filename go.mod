module bitbucket.org/jzmine/go_myutils

go 1.12

require (
	github.com/albrow/jobs v0.4.2
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a
	github.com/astaxie/beego v1.11.1
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/dchest/uniuri v0.0.0-20200228104902-7aecb25e1fe5 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/garyburd/redigo v1.6.0 // indirect
	github.com/go-ini/ini v1.57.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/jinzhu/gorm v1.9.16
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lib/pq v1.1.1
	github.com/matoous/go-nanoid v1.4.1
	github.com/mikespook/gorbac v2.1.0+incompatible
	github.com/nakagami/firebirdsql v0.0.0-20200601120330-692b8e9d4293
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
	github.com/slack-go/slack v0.6.5
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/zabawaba99/fireauth v0.0.0-20160908113251-a96e4680fe07
	github.com/zabawaba99/firego v0.0.0-20190331000051-3bcc4b6a4599 // indirect
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/guregu/null.v3 v3.4.0
	gopkg.in/ini.v1 v1.57.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
	gopkg.in/zabawaba99/firego.v1 v1.0.0-20190331000051-3bcc4b6a4599
)
