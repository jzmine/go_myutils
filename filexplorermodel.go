package go_myutils

import (
	"gopkg.in/guregu/null.v3/zero"
	"time"
)

const (
	FE_DIRECTORY = "Directory"
	FE_FILE      = "File"
)

type FileExplorerReq struct {
	Action, ActionType             string
	CaseSensitive                  bool
	CommonFiles                    string
	ExtensionsAllow                string
	FileUpload                     string
	LocationFrom, LocationTo, Name string
	Names                          []string
	NewName, Path, SearchString    string
	//SelectedItems []string
}

type FileExplorerResp struct {
	Cwd     FECwd   `json:"cwd"`
	Files   []FECwd `json:"files"`
	Details string
	Error   string
}

type FECwd struct {
	Name         string      `json:"name"`
	Type         string      `json:"type"`
	DateModified time.Time   `json:"dateModified"`
	FilterPath   string      `json:"filterPath"`
	IsFile       bool        `json:"isFile"`
	Permission   zero.String `json:"permission"`
	Size         int         `json:"size"`
	HasChild     bool        `json:"hasChild"`
}
