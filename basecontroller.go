package go_myutils

import (
	"bitbucket.org/jzmine/go_myutils/defines"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/context"
	logr "github.com/sirupsen/logrus"
	"html/template"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

type EnumCrudAction int

const (
	ENUM_ACTION_NONE EnumCrudAction = iota
	ENUM_ACTION_CREATE
	ENUM_ACTION_UPDATE
	ENUM_ACTION_READ
	ENUM_ACTION_DELETE
)

var (
	ErrSessionExpired     = errors.New("session expired")
	ErrSessionExpiredId   = 500
	ErrInvalidRequestData = errors.New("invalid request data")
)

//called before main prepare takes place
type IPrePrepare interface {
	PrePrepare() error //returns error so we can stop processing
}

type IPostPrepare interface {
	PostPrepare()
}

type IValidateData interface {
	ValidateData()
}

type IHandleJSONErrorResponse interface {
	HandleJSONErrorResponse(response string, internalResponse *string)
}

type APIError struct {
	Err            error
	Code           int
	DeveloperError error
	InternalError  error `json:"-"`
}

type MyController struct {
	beego.Controller
	PrepareFailed bool
	// static file cache support
	cacheFile                string
	cacheEnable              bool
	cacheBody                []byte
	HtmlId                   string //so we can pass d dynamic htmlid back to the view
	CrudAction               string //to cater for single forms that do crud
	ReturnUrl                string
	SessionExpireRedirectUrl string
	//SessionIgnoreUrls map[string]bool
	CheckSession         bool
	CheckSessionCacheKey string
	CheckSessionCache    cache.Cache
	CheckInternalSession bool
	IsAPI                bool
	DoLogr               bool
	//LogrErr              bool
	EnumCrudAction
	DTOUser
}

func (c *MyController) Init(ct *context.Context, controllerName, actionName string, app interface{}) {
	c.Controller.Init(ct, controllerName, actionName, app)
	c.CheckSession = true //we check by default
	c.IsAPI = false
}

func (c *MyController) Prepare() {
	//print("a")
	if c.EnableXSRF {
		c.Data["xsrfdata"] = template.HTML(c.XSRFFormHTML())
		if c.IsAjax() {
			//check cookie here so we can return json error
			if err := c.checkXSRFToken(); err != nil {
				//bc.StopRun()
				c.ServeJSONError(500, err)
				c.PrepareFailed = true
				c.StopRun()
				return
			}
		}
	}
	c.Data["apptitle"] = beego.AppConfig.String("apptitle")
	if app, ok := c.AppController.(IPrePrepare); ok {
		if err := app.PrePrepare(); err != nil {
			c.ServeJSONError(500, err)
			c.PrepareFailed = true
			c.StopRun()
			return
		}
	}
	//print("b")
	if beego.AppConfig.DefaultBool("cache_template", false) && c.cacheEnable {
		c.cacheFile = path.Join(beego.AppConfig.DefaultString("cache_dir", "_cache"), "en-US", c.Ctx.Input.URL())
		if !strings.HasSuffix(c.cacheFile, ".html") {
			c.cacheFile += ".html"
		}
		if fi, _ := os.Stat(c.cacheFile); fi != nil && !fi.IsDir() && c.cacheEnable {
			http.ServeFile(c.Ctx.ResponseWriter, c.Ctx.Request, c.cacheFile)
			c.StopRun()
		}
	}

	//bc.Layout = "master_layout.html"
	//bc.setProperTemplateFile()
	//bc.HtmlId = this.GetString("hid")
	if !c.IsAPI {
		c.Data["htmlId"] = c.GetString("hid")
		c.Data["urlpath"] = c.Ctx.Request.URL.Path //no params
		c.Data["uri"] = c.Ctx.Request.RequestURI   //complete with params
		c.ReturnUrl = c.GetString("returnurl", "")
	}
	//print("c")
	//bc.CrudAction = this.GetString("action")

	//so we show alerts
	if !c.IsAPI {
		if beego.BConfig.WebConfig.Session.SessionOn {
			intf := c.GetSession(SHOW_ALERT)
			if intf != nil {
				var ok bool
				sa, ok := intf.(ShowAlert)
				if ok {
					c.Data["showAlert"] = true
					c.Data["alertTitle"] = sa.Title
					c.Data["alertType"] = sa.Type
					c.Data["alertMessage"] = sa.Message
					//remove it
					c.DelSession(SHOW_ALERT)
				}
			}

			//here we check the session if found
			if c.CheckSession {
				if err := c.checkSessionUser(); err != nil {
					if c.IsAjax() {
						c.ServeJSONError(defines.SessionExpired, err)
						c.PrepareFailed = true
					} else if c.SessionExpireRedirectUrl != "" && c.IsAjax() == false {
						//at this point, the controller has sent a redirect response
					}
					c.StopRun()
					return
				}
			}
		}
	}
	//print("d")
	/*flash := beego.ReadFromRequest(&c.Controller)
	  if n, ok := flash.Data["ALERT_MESSAGE"]; ok {
	    println(n)
	    bc.Data["showAlert"] = true
	    //bc.Data["alertTitle"]
	  }*/

	if app, ok := c.AppController.(IValidateData); ok {
		app.ValidateData()
	}
	//print("e")

	if app, ok := c.AppController.(IPostPrepare); ok {
		app.PostPrepare()
	}
	//print("f")

	if !c.IsAPI {
		//get more varialbes
		switch c.GetString("act") {
		case "":
			c.EnumCrudAction = ENUM_ACTION_NONE
		case CRUD_CREATE:
			c.EnumCrudAction = ENUM_ACTION_CREATE
		case CRUD_DELETE:
			c.EnumCrudAction = ENUM_ACTION_DELETE
		case CRUD_READ:
			c.EnumCrudAction = ENUM_ACTION_READ
		case CRUD_UPDATE:
			c.EnumCrudAction = ENUM_ACTION_UPDATE
		}
	}
	//print("g")
}

func (c *MyController) checkXSRFToken() (err error) {
	r := c.Ctx.Request
	inp := c.Ctx.Input
	if r.Method == http.MethodPost || r.Method == http.MethodDelete || r.Method == http.MethodPut ||
		(r.Method == http.MethodPost && (inp.Query("_method") == http.MethodDelete || inp.Query("_method") == http.MethodPut)) {

		hdr := r.Header
		token := inp.Query("_xsrf")
		if token == "" {
			token = hdr.Get("X-Xsrftoken")
		}
		if token == "" {
			token = hdr.Get("X-Csrftoken")
		}
		if token == "" {
			return defines.ErrInvalidRequestData
		}
		if c.XSRFToken() != token {
			return defines.ErrInvalidRequestData
		}
	}

	return nil
}

func (c *MyController) Render() error {
	if beego.AppConfig.DefaultBool("cache_template", false) && c.cacheEnable {
		if renderBytes, _ := c.RenderBytes(); len(renderBytes) > 0 {
			c.cacheBody = renderBytes
		}
	}
	return c.Controller.Render()
}

func (c *MyController) Finish() {
	if c.cacheEnable {
		os.MkdirAll(path.Dir(c.cacheFile), os.ModePerm)
		ioutil.WriteFile(c.cacheFile, c.cacheBody, os.ModePerm) // todo : error handle
	}
}

// ServeValidationErrors prepares and serves a validation exception.
func (c *MyController) ServeValidationErrors(Errors []string) {
	c.Data["json"] = struct {
		Errors []string `json:"errors"`
	}{Errors}
	c.Ctx.Output.SetStatus(500)
	c.ServeJSON()
}

func (c *MyController) ServeError(code int, err error) {
	pc, fn, line, _ := runtime.Caller(1) //log.Printf("[error] %s:%d %v", fn, line, err)
	logr.WithFields(logr.Fields{"pc": runtime.FuncForPC(pc).Name(), "fn": fn, "line": line}).Error(err)
	beego.Error(err)
	serr := err.Error()
	c.Data["json"] = map[string]interface{}{"error": serr}
	c.Ctx.Output.EnableGzip = false
	c.ServeJSON()
	c.Ctx.Output.EnableGzip = true
}

func (c *MyController) ServeJSONAPIError(apiErr *APIError) {
	c.ServeJSONError(apiErr.Code, apiErr)
	return
	/*prepareErrorResponse(c, apiErr.Code, apiErr.Err)

	var content []byte
	var jsErr error
	c.Ctx.ResponseWriter.WriteHeader(apiErr.Code)
	data := map[string]interface{}{"success": false, "error": apiErr.Err.Error(),
		"isError": true, "errorCode": apiErr.Code}
	if apiErr.DeveloperError != nil {
		data["developerErr"] = apiErr.DeveloperError.Error()
	}
	content, jsErr = json.Marshal(data)

	if jsErr != nil {
		fmt.Fprintln(c.Ctx.ResponseWriter, jsErr.Error())
		if app, ok := c.AppController.(IHandleJSONErrorResponse); ok {
			var internalError *string
			internalError = nil
			if apiErr.InternalError != nil {
				*internalError = apiErr.InternalError.Error()
			}
			app.HandleJSONErrorResponse(jsErr.Error(), internalError)
		}
	} else {
		fmt.Fprintln(c.Ctx.ResponseWriter, string(content))
		if app, ok := c.AppController.(IHandleJSONErrorResponse); ok {
			app.HandleJSONErrorResponse(string(content))
		}
	}
	return*/
}

func prepareErrorResponse(c *MyController, code int, err error) {
	if c.DoLogr {
		pc, fn, line, _ := runtime.Caller(1) //log.Printf("[error] %s:%d %v", fn, line, err)
		logr.WithFields(logr.Fields{"pc": runtime.FuncForPC(pc).Name(), "fn": fn, "line": line}).Error(err)
	}
	beego.Error(err)
	c.Ctx.ResponseWriter.Header().Set("Content-Type", "application/json; charset=utf-8")
	c.Ctx.ResponseWriter.Header().Set("X-Content-Type-Options", "nosniff")
	//so callers can handle us specially
	c.Ctx.ResponseWriter.Header().Set("Go-Error", "1")
	//in situations where code < 500, make it 500
	c.Ctx.ResponseWriter.WriteHeader(code)
}

func (c *MyController) ServeJSONError(code int, err interface{}) {
	var apiErr *APIError
	var apiErr2 APIError

	//var er error
	//var ok bool
	er, ok := err.(error)
	if ok {
		prepareErrorResponse(c, code, er)
	} else {
		//err, ok1 :=
		apiErr, ok = err.(*APIError)
		if !ok {
			apiErr2, ok = err.(APIError)
			if ok {
				apiErr = &apiErr2
			}
		}
		if ok {
			prepareErrorResponse(c, apiErr.Code, apiErr.Err)
		}
	}
	/*if c.DoLogr {
		pc, fn, line, _ := runtime.Caller(1) //log.Printf("[error] %s:%d %v", fn, line, err)
		logr.WithFields(logr.Fields{"pc": runtime.FuncForPC(pc).Name(), "fn": fn, "line": line}).Error(err)
	}
	beego.Error(err)
	c.Ctx.ResponseWriter.Header().Set("Content-Type", "application/json; charset=utf-8")
	c.Ctx.ResponseWriter.Header().Set("X-Content-Type-Options", "nosniff")
	//so callers can handle us specially
	c.Ctx.ResponseWriter.Header().Set("Go-Error", "1")
	//in situations where code < 500, make it 500
	c.Ctx.ResponseWriter.WriteHeader(code)
	*/

	var content []byte
	var jserr error

	if jsdata, ok := err.(map[string]interface{}); ok {
		content, jserr = json.Marshal(jsdata)
		var jsStr string
		if jserr != nil {
			jsStr = jserr.Error()
		} else {
			//fmt.Fprintln(c.Ctx.ResponseWriter, string(content))
			jsStr = string(content)
		}
		fmt.Fprintln(c.Ctx.ResponseWriter, jsStr)
		if app, ok := c.AppController.(IHandleJSONErrorResponse); ok {
			app.HandleJSONErrorResponse(jsStr, nil)
		}
		return
	}

	//var apiErr *APIError
	//var apiErr2 APIError
	//var ok bool
	apiErr, ok = err.(*APIError)
	if !ok {
		apiErr2, ok = err.(APIError)
		if ok {
			apiErr = &apiErr2
		}
	}
	if ok {
		c.Ctx.ResponseWriter.WriteHeader(apiErr.Code)
		data := map[string]interface{}{"success": false, "error": apiErr.Err.Error(),
			"isError": true, "errorCode": apiErr.Code}
		if apiErr.DeveloperError != nil {
			data["developerErr"] = apiErr.DeveloperError.Error()
		}
		content, jserr = json.Marshal(data)
		var jsStr string
		if jserr != nil {
			jsStr = jserr.Error()
			//fmt.Fprintln(c.Ctx.ResponseWriter, jserr.Error())
		} else {
			//fmt.Fprintln(c.Ctx.ResponseWriter, string(content))
			jsStr = string(content)
		}
		fmt.Fprintln(c.Ctx.ResponseWriter, jsStr)
		if app, ok := c.AppController.(IHandleJSONErrorResponse); ok {
			var internalError *string
			internalError = nil
			if apiErr.InternalError != nil {
				s := apiErr.InternalError.Error()
				internalError = &s
			}
			app.HandleJSONErrorResponse(jsStr, internalError)
		}

		return
	}

	if er, ok := err.(error); ok {
		msg := strings.Replace(er.Error(), "pq:", "", 1)
		content, jserr = json.Marshal(map[string]interface{}{"success": false, "error": msg /*er.Error()*/, "isError": true,
			"session_expired": err == ErrSessionExpired})
		var jsStr string
		if jserr != nil {
			jsStr = jserr.Error()
			//fmt.Fprintln(c.Ctx.ResponseWriter, jserr.Error())
		} else {
			jsStr = string(content)
			//fmt.Fprintln(c.Ctx.ResponseWriter, string(content))
		}
		fmt.Fprintln(c.Ctx.ResponseWriter, jsStr)
		if app, ok := c.AppController.(IHandleJSONErrorResponse); ok {
			app.HandleJSONErrorResponse(jsStr, nil)
		}
		return
	}
	if ers, ok := err.(govalidator.Errors); ok {
		//build array of errors
		var serrors []string
		for _, er := range ers {
			serrors = append(serrors, er.Error())
		}
		content, jserr = json.Marshal(map[string]interface{}{"success": false, "errors": strings.Join(serrors, "^$^")})
		var jsStr string
		if jserr != nil {
			jsStr = jserr.Error()
			//fmt.Fprintln(c.Ctx.ResponseWriter, jserr.Error())
		} else {
			jsStr = string(content)
			//fmt.Fprintln(c.Ctx.ResponseWriter, string(content))
		}
		fmt.Fprintln(c.Ctx.ResponseWriter, jsStr)
		if app, ok := c.AppController.(IHandleJSONErrorResponse); ok {
			app.HandleJSONErrorResponse(jsStr, nil)
		}
		return
	}

	//http.Error(bc.Ctx.ResponseWriter, err.Error(), code)
}

//added to cater for returning input data with the error
func (c *MyController) ServeJSONErrorWithData(code int, err interface{}, data interface{}) {
	jserr := map[string]interface{}{"success": false, "isError": true, "session_expired": err == ErrSessionExpired,
		"error": strings.Replace((err.(error)).Error(), "pq:", "", 1), "data": data}
	c.ServeJSONError(code, jserr)
}

func (c *MyController) ServeStandardJSONError(code int, err error) {
	pc, fn, line, _ := runtime.Caller(1) //log.Printf("[error] %s:%d %v", fn, line, err)
	logr.WithFields(logr.Fields{"pc": runtime.FuncForPC(pc).Name(), "fn": fn, "line": line}).Error(err)
	beego.Error(err)
	c.Ctx.ResponseWriter.Header().Set("Content-Type", "application/json; charset=utf-8")
	c.Ctx.ResponseWriter.Header().Set("X-Content-Type-Options", "nosniff")
	c.Ctx.ResponseWriter.WriteHeader(code)
	var content []byte
	js := map[string]interface{}{
		"error": map[string]interface{}{
			"message": err.Error(),
			"code":    code,
		}, "success": false,
	}
	content, err = json.Marshal(js)
	if err != nil {
		fmt.Fprintln(c.Ctx.ResponseWriter, err.Error())
	} else {
		fmt.Fprintln(c.Ctx.ResponseWriter, string(content))
	}
}

func (c *MyController) ServeErrors(code int, err []error) {
	pc, fn, line, _ := runtime.Caller(1) //log.Printf("[error] %s:%d %v", fn, line, err)
	logr.WithFields(logr.Fields{"pc": runtime.FuncForPC(pc).Name(), "fn": fn, "line": line}).Error(err)
	beego.Error(err)
	c.Ctx.Output.SetStatus(code)
	if len(err) > 0 {
		var aerrs []map[string]interface{}
		for _, er := range err {
			aerrs = append(aerrs, map[string]interface{}{"error": er.Error()})
		}
		c.Data["json"] = aerrs
	} else {
		c.Data["json"] = map[string]interface{}{"error": "Unknown Error!"}
	}
	c.Ctx.Output.EnableGzip = false
	c.ServeJSON()
	c.Ctx.Output.EnableGzip = true
}

//** CATCHING PANICS

// CatchPanic is used to catch any Panic and log exceptions. Returns a 500 as the response.
func (c *MyController) CatchPanic(functionName string) {
	if r := recover(); r != nil {
		buf := make([]byte, 10000)
		runtime.Stack(buf, false)
		beego.Warning(functionName, "PANIC Defered [%v] : Stack Trace : %v", r, string(buf))
		c.ServeError(500, fmt.Errorf("%v", r))
	}
}

//** AJAX SUPPORT

// AjaxResponse returns a standard ajax response.
func (c *MyController) AjaxResponse(resultCode int, resultString string, data interface{}) {
	response := struct {
		Result       int
		ResultString string
		ResultObject interface{}
	}{
		Result:       resultCode,
		ResultString: resultString,
		ResultObject: data,
	}

	c.Data["json"] = response
	c.ServeJSON()
}

func (c *MyController) ServeJSONSuccess() {
	c.Data["json"] = map[string]interface{}{
		"result": true,
	}
	c.ServeJSON()
}

var (
	ErrNoImageUploaded    = errors.New("No image uploaded")
	ErrInvalidImageFormat = errors.New("Invalid image format!")
	ErrInvalidFileSize    = errors.New("Invalid file size")
	ErrFileSaveError      = errors.New("Error saving image file")
)

func (c *MyController) SaveImageFile(htmlKey string, exts map[string]bool, size int64,
	destfname, destfolder string) (fext string, err error) {
	var mfh *multipart.FileHeader
	var mf multipart.File

	if mf, mfh, err = c.GetFile(htmlKey); err != nil { //&& err != http.ErrMissingFile {
		return "", ErrNoImageUploaded
	}
	fext = filepath.Ext(mfh.Filename)
	if !exts[fext] { //fext != ".jpg" && ext != ".jpeg" && ext != ".jpg" {
		return "", ErrInvalidImageFormat //errors.New("Invalid image format. Only JPG, JPEG or PNG allowed!")
	}
	//chk size
	type Sizer interface {
		Size() int64
	}
	if fsize := mf.(Sizer).Size(); fsize > size {
		return "", ErrInvalidFileSize
	}

	defer mf.Close()

	var dfilename = fmt.Sprintf("%s%s%s", destfolder, destfname, fext)
	var destfile *os.File
	if destfile, err = os.OpenFile(dfilename, os.O_WRONLY|os.O_CREATE, 0666); err != nil {
		SlackNotifyError(err)
		return "", ErrFileSaveError
	}
	//println(dfilename)
	defer destfile.Close()
	io.Copy(destfile, mf)

	return fext, nil
}

// struct for session user info
type DTOUser struct {
	Id        int64
	Uid       string
	Username  string
	Fullnames string
	Roles     []string
}

// check if the user exists in the session
func (c *MyController) checkSessionUser() (err error) {
	if c.CheckSessionCacheKey == "" {
		panic(errors.New("CheckSessionCacheKey not set"))
	}
	if c.CheckSessionCache == nil {
		panic(errors.New("CheckSessionCache not set"))
	}
	var fnexpired = func() error {
		if c.IsAjax() {
			//c.ServeJSONError(defines.SESSION_EXPIRED, ErrSessionExpired)
			return ErrSessionExpired
		} else {
			c.Redirect(c.SessionExpireRedirectUrl, 302)
		}
		return defines.ErrBlank
	}

	var uid string
	//check session for user
	if c.CheckInternalSession {
		uid, ok := c.GetSession(c.CheckSessionCacheKey).(string)
		if uid == "" || !ok {
			print("uid not found in GetSession")
			return fnexpired()
		}
	}

	if uid == "" {
		//load the uid from header token key
		uid = c.Ctx.Request.Header.Get("token")
		if uid == "" {
			return errors.New("Access token not found!")
		}
	}
	//validate it by checking if in cache
	if !c.CheckSessionCache.IsExist(uid) {
		print("uid not cached not found in CheckSessionCache")
		return fnexpired()
	}

	//now load d user
	jsonuser := c.CheckSessionCache.Get(uid).([]byte)
	err = json.Unmarshal(jsonuser, &c.DTOUser)
	return err
}

func (c *MyController) Trace() {
	panic("implement me")
}
