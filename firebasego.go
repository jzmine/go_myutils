package go_myutils

import (
	"encoding/json"
	"errors"
	"github.com/zabawaba99/fireauth"
	"gopkg.in/zabawaba99/firego.v1"
	"io/ioutil"
	"os"
	"time"
	//"log"
	"fmt"
	"github.com/dgrijalva/jwt-go"
)

func FirebaseGetToken(secret string, data fireauth.Data) (token string, err error) {
	now := time.Now().Unix()
	options := &fireauth.Option{
		NotBefore: now - 30,
		//TODO test how to handle token expiration
		Expiration: now + (60*60)*24, // Maximum expiration time is one day
		Admin:      true,
		Debug:      true,
	}
	gen := fireauth.New(secret)
	token, err = gen.CreateToken(data, options)
	if err != nil {
		go SlackNotifyError(err)
		return "", errors.New("System Error! Please retry in a few minutes")
		//log.Fatal(err )
	}
	return token, err
}

func firebaseSaveData(ffb *firego.Firebase, path string, data map[string]interface{}, doUpdate bool) (
	fbRef *firego.Firebase, err error) {
	saver := func() error {
		fbRef, err = ffb.Ref(path)
		if err != nil {
			return err
		}
		if doUpdate {
			if err := fbRef.Update(data); err != nil {
				return err
			}
		} else {
			if err := fbRef.Set(data); err != nil {
				return err
			}
		}

		return err
	}
	err = saver()
	//TODO this should be retried if error is authentication expired
	if err != nil {
		go SlackNotifyError(err)
		return nil, err
	}
	return fbRef, err
}

func FirebaseInsertData(ffb *firego.Firebase, path string, data map[string]interface{}) (
	fbRef *firego.Firebase, err error) {
	return firebaseSaveData(ffb, path, data, false)
}

func FirebaseUpdateData(ffb *firego.Firebase, path string, data map[string]interface{}) (
	fbRef *firego.Firebase, err error) {
	return firebaseSaveData(ffb, path, data, true)
}

type firebaseInfo struct {
	AuthProviderX509CertURL string `json:"auth_provider_x509_cert_url"`
	AuthURI                 string `json:"auth_uri"`
	ClientEmail             string `json:"client_email"`
	ClientID                string `json:"client_id"`
	ClientX509CertURL       string `json:"client_x509_cert_url"`
	PrivateKey              string `json:"private_key"`
	PrivateKeyID            string `json:"private_key_id"`
	ProjectID               string `json:"project_id"`
	TokenURI                string `json:"token_uri"`
	Type                    string `json:"type"`
}

func FirebaseGetUserLoginToken(uid interface{}) (token string, err error) {
	err = func() error {
		fbjsonfile := os.Getenv("FIREBASE_JSON_JSON")
		if fbjsonfile == "" {
			panic(errors.New("FB Json environment not set"))
		}
		fbInfoContent, err := ioutil.ReadFile(fbjsonfile)
		if err != nil {
			return err
		}

		// Unmarshal json into fbInfo.
		var fbInfo firebaseInfo
		if err = json.Unmarshal(fbInfoContent, &fbInfo); err != nil {
			//log.Fatal(err)
			return err
		}

		// Create jwtToken with SigningMethodHS256.
		now := time.Now().Unix()
		jwtToken := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
			"iss": fbInfo.ClientEmail,
			"sub": fbInfo.ClientEmail,
			"aud": "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
			"iat": now,
			"exp": now + (60 * 60), // Maximum expiration time is one day
			"uid": uid,
		})

		// Create signKey by parsing RSA private key.
		signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(fbInfo.PrivateKey))
		if err != nil {
			//log.Fatal("error parsing rsa", err)
			return errors.New("error parsing rsa" + err.Error())
		}

		// Generate token with signKey.
		token, err = jwtToken.SignedString(signKey)
		if err != nil {
			//log.Fatal("error signing string", err)
			return errors.New("error signing string" + err.Error())
		}

		// Finally, after 100000 google searches my problem may be solved.
		// Thank you heaps for helping me! :)
		fmt.Println("SUCCESS:", token)
		return nil
	}()

	if err != nil {
		go SlackNotifyError(err)
	}
	return token, nil
}
