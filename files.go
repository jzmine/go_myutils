package go_myutils

import (
	"fmt"
	"github.com/pkg/errors"
	"os"
)

func FindFirstFileViaExt(filePath string, findFilename string, exts []string) (foundFile string, err error) {
	//find file 1st in case it has extension
	foundFile = fmt.Sprintf("%s%s", filePath, findFilename)
	if _, err = os.Stat(foundFile); err == nil {
		return foundFile, nil
	}
	for _, ext := range exts {
		foundFile = fmt.Sprintf("%s%s.%s", filePath, findFilename, ext)
		if _, err = os.Stat(foundFile); err == nil {
			return foundFile, nil
		}
	}

	return "", errors.New("File not found!")
}
