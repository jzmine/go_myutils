package go_myutils

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

var (
	APIErrorServerError = &APIError{
		Err:  errors.New("temporary server error. please try again"),
		Code: 503,
	}

	APIErrorServerErrorMessage = func(msg string) *APIError {
		return &APIError{
			Err:  errors.New(fmt.Sprint("Server error ", msg)),
			Code: 503,
		}
	}

	APIErrorInvalidRequestJSONData = &APIError{
		Err:  errors.New("invalid json request data"),
		Code: 501,
	}
	APIErrorInvalidRequestData = &APIError{
		Err:  errors.New("invalid request data"),
		Code: 502,
	}
	APIErrorInvalidRequestDataField = func(fieldName string, fieldValue interface{}) *APIError {
		return &APIError{
			Err:  errors.New(fmt.Sprint("Invalid request value ", fieldValue, " for field ", fieldName)),
			Code: 504,
		}
	}
	APIErrorPaymentValidationFailed = func(transactionReference string) *APIError {
		return &APIError{
			Err:  errors.New(fmt.Sprint("error validating your payment for transaction %s", transactionReference)),
			Code: 505,
		}
	}
)

func HttpResponseToAPIError(response *http.Response) (apiErr *APIError) {
	var jsErr map[string]interface{}
	defer response.Body.Close() ///****
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error(errors.Wrap(err, "reading response"))
		return APIErrorServerError
	}
	return HttpResponseToAPIErrorFromBody(bodyBytes, response)

	err = json.Unmarshal(bodyBytes, &jsErr)
	errCode := response.StatusCode
	var strErr string
	var ok bool
	//speedup. return error if it contains developerCode
	//{"developerErr":"error connecting to AEDC","error":"temporary server error. please try again later","errorCode":560,"isError":true,"success":false}
	if _, ok = jsErr["developerErr"]; ok {
		apiErr = &APIError{
			Err:            errors.New(jsErr["error"].(string)),
			Code:           int(jsErr["errorCode"].(float64)),
			DeveloperError: errors.New(jsErr["developerErr"].(string)),
		}
		//json.Unmarshal(bodyBytes, &apiErr)
		return apiErr
	}
	var intfErr map[string]interface{}

	strErr, ok = jsErr["error"].(string)
	if !ok {
		intfErr, ok = jsErr["error"].(map[string]interface{})
		if ok {
			strErr = intfErr["message"].(string)
			errCode = int(intfErr["code"].(float64))
		}
	}
	apiErr = &APIError{
		Err:  errors.New(strErr), //errors.New(jsErr["error"].(string)),
		Code: errCode,            //response.StatusCode,
	}

	return apiErr
}

func HttpResponseToAPIErrorFromBody(bodyBytes []byte, response *http.Response) (apiErr *APIError) {
	var jsErr map[string]interface{}
	if err := json.Unmarshal(bodyBytes, &jsErr); err != nil {
		apiErr = APIErrorServerError
		apiErr.InternalError = err
		return
	}

	errCode := response.StatusCode
	var strErr string
	var ok bool
	//speedup. return error if it contains developerCode
	//{"developerErr":"error connecting to AEDC","error":"temporary server error. please try again later","errorCode":560,"isError":true,"success":false}
	if _, ok = jsErr["developerErr"]; ok {
		apiErr = &APIError{
			Err:            errors.New(jsErr["error"].(string)),
			Code:           int(jsErr["errorCode"].(float64)),
			DeveloperError: errors.New(jsErr["developerErr"].(string)),
		}
		//json.Unmarshal(bodyBytes, &apiErr)
		return apiErr
	}
	var intfErr map[string]interface{}

	strErr, ok = jsErr["error"].(string)
	if !ok {
		intfErr, ok = jsErr["error"].(map[string]interface{})
		if ok {
			strErr = intfErr["message"].(string)
			errCode = int(intfErr["code"].(float64))
		}
	}
	apiErr = &APIError{
		Err:  errors.New(strErr), //errors.New(jsErr["error"].(string)),
		Code: errCode,            //response.StatusCode,
	}

	return apiErr
}
