package publitio
import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"math/big"
	"os"
	"strconv"
	"time"
)

func GenerateSignature(secret, timestamp, nonce string) string {
	sum := sha1.Sum([]byte(timestamp + nonce + secret))
	hexSum := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(hexSum, sum[:])
	return string(hexSum[:])
}

func GenerateNonce() (string, error) {
	r, err := rand.Int(rand.Reader, big.NewInt(89999999))
	if err != nil {
		return "", fmt.Errorf("error while generating nonce: %v", err)
	}
	r.Add(r, big.NewInt(10000000))
	return r.String(), nil
}

func GetSignatureNonceTimestamp() (nonce string, timestamp string, sig string, err error)  {
	timestamp = strconv.FormatInt(time.Now().Unix()%0xFFFFFFFF, 10)
	nonce, err = GenerateNonce()
	if err != nil {
		return "", "", "", err
	}
	sig = GenerateSignature(os.Getenv("PUBLITIO_SECRET"), timestamp, nonce)
	return
}
