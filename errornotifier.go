package go_myutils

import (
	"fmt"
	//"github.com/nlopes/slack"
	"errors"
	"github.com/slack-go/slack"
)

var (
	ErrOperationFailedTryAgain = errors.New("Operation failed. Try again")
)

var SlackAPIClient *slack.Client = nil
var logChannel string // = "beego_logs"

func SetupSlackAPI(apiToken, channelName string) {
	//token := os.Getenv(apiTokenEnvName)
	//if token == "" {
	//  panic(errors.New("SlackAPI token not set"))
	//}
	SlackAPIClient = slack.New(apiToken, slack.OptionDebug(true))
	logChannel = channelName //os.Getenv(channelName)
}

func sendErrorToSlack(err error) {
	params := slack.MsgOptionText(err.Error(), true) //slack.PostMessageParameters{}

	_, _, err = SlackAPIClient.PostMessage(logChannel, params)
	if err != nil {
		fmt.Printf("Slack->sendErrorToSlack%s\n", err)
	}
}

func SlackSendMessage(channel string, msg string) {
	if channel == "" {
		channel = logChannel
	}
	params := slack.MsgOptionText(msg, true) //slack.PostMessageParameters{}
	var err error
	_, _, err = SlackAPIClient.PostMessage(channel, params)
	if err != nil {
		fmt.Printf("Slack->SendMssage: %s\n", err)
	}
}

func SlackNotifyError(err error) {
	if SlackAPIClient != nil {
		sendErrorToSlack(err)
	}
	fmt.Printf("NotifyError: %#v\n", err)
}
