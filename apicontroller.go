package go_myutils

import "github.com/astaxie/beego/context"

type APIBaseController struct {
	MyController
	ApiLogId         int64
	InternalResponse *string
}

func (c *APIBaseController) Init(ct *context.Context, controllerName, actionName string, app interface{}) {
	c.MyController.Init(ct, controllerName, actionName, app)
	//c.LogrErr = true
	c.IsAPI = true
	c.EnableXSRF = false
	c.cacheEnable = false
}
