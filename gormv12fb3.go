package go_myutils
//
//import (
//	//"context"
//	"database/sql"
//	"fmt"
//	_ "github.com/nakagami/firebirdsql"
//	"github.com/jinzhu/gorm"
//	"strings"
//)
//
//type Config struct {
//	DriverName                string
//	DSN                       string
//	Conn                      *sql.DB //gorm.ConnPool
//	//cn *sql.DB
//	SkipInitializeWithVersion bool
//	//DefaultStringSize         uint
//	//DefaultDatetimePrecision  *int
//	//DisableDatetimePrecision  bool
//	//DontSupportRenameIndex    bool
//	//DontSupportRenameColumn   bool
//	//DontSupportForShareClause bool
//}
//
//type Dialector struct {
//	*Config
//}
//
//func Open(dsn string) gorm.Dialector {
//	return &Dialector{Config: &Config{DSN: dsn}}
//}
//
//func New(config Config) gorm.Dialector {
//	return &Dialector{Config: &config}
//}
//
//func (dialector Dialector) Name() string {
//	return "firebirdsql"
//}
//
//func (dialector Dialector) Initialize(db *gorm.DB) (err error) {
//	//ctx := context.Background()
//
//	// register callbacks
//	callbacks.RegisterDefaultCallbacks(db, &callbacks.Config{WithReturning: true})
//
//	//db.Callback().Update().Replace("gorm:update", Update)
//
//	if dialector.DriverName == "" {
//		dialector.DriverName = "mysql"
//	}
//
//	//if dialector.DefaultDatetimePrecision == nil {
//	//	var defaultDatetimePrecision = 3
//	//	dialector.DefaultDatetimePrecision = &defaultDatetimePrecision
//	//}
//
//	if dialector.Conn != nil {
//		db.ConnPool = dialector.Conn
//	} else {
//		db.ConnPool, err = sql.Open(dialector.DriverName, dialector.DSN)
//		if err != nil {
//			return err
//		}
//	}
//
//	/*if !dialector.Config.SkipInitializeWithVersion {
//		var version string
//		err = db.ConnPool.QueryRowContext(ctx, "SELECT VERSION()").Scan(&version)
//		if err != nil {
//			return err
//		}
//
//		if strings.Contains(version, "MariaDB") {
//			dialector.Config.DontSupportRenameIndex = true
//			dialector.Config.DontSupportRenameColumn = true
//			dialector.Config.DontSupportForShareClause = true
//		} else if strings.HasPrefix(version, "5.6.") {
//			dialector.Config.DontSupportRenameIndex = true
//			dialector.Config.DontSupportRenameColumn = true
//			dialector.Config.DontSupportForShareClause = true
//		} else if strings.HasPrefix(version, "5.7.") {
//			dialector.Config.DontSupportRenameColumn = true
//			dialector.Config.DontSupportForShareClause = true
//		} else if strings.HasPrefix(version, "5.") {
//			dialector.Config.DisableDatetimePrecision = true
//			dialector.Config.DontSupportRenameIndex = true
//			dialector.Config.DontSupportRenameColumn = true
//			dialector.Config.DontSupportForShareClause = true
//		}
//	}*/
//
//	/*for k, v := range dialector.ClauseBuilders() {
//		db.ClauseBuilders[k] = v
//	}*/
//	return
//}
///*
//func (dialector Dialector) ClauseBuilders() map[string]clause.ClauseBuilder {
//	clauseBuilders := map[string]clause.ClauseBuilder{
//		"ON CONFLICT": func(c clause.Clause, builder clause.Builder) {
//			if onConflict, ok := c.Expression.(clause.OnConflict); ok {
//				builder.WriteString("ON DUPLICATE KEY UPDATE ")
//				if len(onConflict.DoUpdates) == 0 {
//					if s := builder.(*gorm.Statement).Schema; s != nil {
//						var column clause.Column
//						onConflict.DoNothing = false
//
//						if s.PrioritizedPrimaryField != nil {
//							column = clause.Column{Name: s.PrioritizedPrimaryField.DBName}
//						} else if len(s.DBNames) > 0 {
//							column = clause.Column{Name: s.DBNames[0]}
//						}
//
//						if column.Name != "" {
//							onConflict.DoUpdates = []clause.Assignment{{Column: column, Value: column}}
//						}
//					}
//				}
//
//				for idx, assignment := range onConflict.DoUpdates {
//					if idx > 0 {
//						builder.WriteByte(',')
//					}
//
//					builder.WriteQuoted(assignment.Column)
//					builder.WriteByte('=')
//					if column, ok := assignment.Value.(clause.Column); ok && column.Table == "excluded" {
//						column.Table = ""
//						builder.WriteString("VALUES(")
//						builder.WriteQuoted(column)
//						builder.WriteByte(')')
//					} else {
//						builder.AddVar(builder, assignment.Value)
//					}
//				}
//			} else {
//				c.Build(builder)
//			}
//		},
//		"VALUES": func(c clause.Clause, builder clause.Builder) {
//			if values, ok := c.Expression.(clause.Values); ok && len(values.Columns) == 0 {
//				builder.WriteString("VALUES()")
//				return
//			}
//			c.Build(builder)
//		},
//	}
//
//	if dialector.Config.DontSupportForShareClause {
//		clauseBuilders["FOR"] = func(c clause.Clause, builder clause.Builder) {
//			if values, ok := c.Expression.(clause.Locking); ok && strings.EqualFold(values.Strength, "SHARE") {
//				builder.WriteString("LOCK IN SHARE MODE")
//				return
//			}
//			c.Build(builder)
//		}
//	}
//
//	return clauseBuilders
//}
//*/
//func (dialector Dialector) DefaultValueOf(field *schema.Field) clause.Expression {
//	return clause.Expr{SQL: "DEFAULT"}
//}
//
//func (dialector Dialector) Migrator(db *gorm.DB) gorm.Migrator {
//	return migrator.Migrator{
//			Config: migrator.Config{
//				DB:        db,
//				Dialector: dialector,
//				CreateIndexAfterCreateTable: true,
//			},
//		}
//
//}
//
//func (dialector Dialector) BindVarTo(writer clause.Writer, stmt *gorm.Statement, v interface{}) {
//	//writer.WriteByte('?')
//	writer.WriteString("?")
//}
//
//func (dialector Dialector) QuoteTo(writer clause.Writer, str string) {
//	writer.WriteString(fmt.Sprintf(`%s`, strings.ToLower(str)))
//	/*writer.WriteByte('`')
//	if strings.Contains(str, ".") {
//		for idx, str := range strings.Split(str, ".") {
//			if idx > 0 {
//				writer.WriteString(".`")
//			}
//			writer.WriteString(str)
//			writer.WriteByte('`')
//		}
//	} else {
//		writer.WriteString(str)
//		writer.WriteByte('`')
//	}*/
//}
//
//func (dialector Dialector) Explain(sql string, vars ...interface{}) string {
//	return logger.ExplainSQL(sql, nil, `'`, vars...)
//}
//
//func (dialector Dialector) DataTypeOf(field *schema.Field) string {
//	switch field.DataType {
//	case schema.Bool:
//		return "BOOLEAN"
//	case schema.Int, schema.Uint:
//		size := field.Size
//		if field.DataType == schema.Uint {
//			size++
//		}
//		if field.AutoIncrement {
//			switch {
//			case size <= 16:
//				return "integer generated by default as identify"
//			default:
//				return "bigint GENERATED BY DEFAULT AS IDENTITy"
//			}
//		} else {
//			switch {
//			case size <= 16:
//				return "integer"
//			default:
//				return "bigint"
//			}
//		}
//
//	case schema.Float:
//		if field.Precision > 0 {
//			return fmt.Sprintf("decimal(%d, %d)", field.Precision, field.Scale)
//		}
//		return "float"
//	case schema.String:
//		size := field.Size
//		if size > 0 && size < 65532 {
//			return fmt.Sprintf("varchar(%d)", size)
//		}
//		return "varchar(65532)"
//	case schema.Time:
//		return "TIMESTAMP"
//	case schema.Bytes:
//		if field.Size > 0 && field.Size < 65532 {
//			return "BLOB SUB_TYPE 1 SEGMENT SIZE 80"
//		}
//		return "BLOB SUB_TYPE 0 SEGMENT SIZE 80"
//	}
//
//	return string(field.DataType)
//}
//
//func (dialectopr Dialector) SavePoint(tx *gorm.DB, name string) error {
//	tx.Exec("SAVEPOINT " + name)
//	return nil
//}
//
//func (dialectopr Dialector) RollbackTo(tx *gorm.DB, name string) error {
//	tx.Exec("ROLLBACK TO SAVEPOINT " + name)
//	return nil
//}